;;;; unobscure-minecraft.asd
;;
;;;; Copyright (c) 2020 Gnuxie <Gnuxie@protonmail.com>


(asdf:defsystem #:unobscure-minecraft
  :description "unobscure-minecraft."
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :license  "Artistic License 2.0"
  :version "0.0.1"
  :depends-on ("alexandria" "cl-ppcre" "str")
  :serial t
  :components ((:file "package")
               (:file "unobscure-minecraft")
               (:file "reader")
               (:file "java-interaction")))
