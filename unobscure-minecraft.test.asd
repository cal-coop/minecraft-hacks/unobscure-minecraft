(asdf:defsystem #:unobscure-minecraft.test
  :description "tests for unobscure-minecraft"
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :license  "Artistic License 2.0"
  :version "0.0.1"
  :depends-on ("parachute" "unobscure-minecraft")
  :serial t
  :components ((:module "test" :components
                        ((:file "package")
                         (:file "reader")))))
