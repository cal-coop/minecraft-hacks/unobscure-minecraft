;;;; package.lisp
;;
;;;; Copyright (c) 2020 Gnuxie <Gnuxie@protonmail.com>


(defpackage #:unobscure-minecraft
  (:use #:cl)
  (:export
   ;; reader
   #:read-class
   #:read-method-or-property
   #:read-table
   ;; core
   #:*class-table*
   #:%find-place
   #:find-jmethod
   #:find-property
   #:obscure-entity
   #:obscure-name
   #:clear-name
   #:obscure-class
   #:obscure-properties
   #:obscure-methods
   #:obscure-place
   #:java-type
   #:obscure-method
   #:parameters
   #:add-obscure-entity
   #:add-to-class
   ;; java interaction
   #:oclass
   #:omethod
   ))
