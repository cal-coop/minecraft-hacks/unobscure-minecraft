;;;; unobscure-minecraft.lisp
;;
;;;; Copyright (c) 2020 Gnuxie <Gnuxie@protonmail.com>


(in-package #:unobscure-minecraft)

(defvar *class-table* (make-hash-table :test 'equal))

(defun %find-place (class-name place-name class-accessor &key (key #'clear-name))
  (let ((class (gethash class-name *class-table*)))
    (when class
      (find place-name (funcall class-accessor class) :test #'string= :key key))))

(defun find-jmethod (class-name method)
  (%find-place class-name method #'obscure-methods))

(defun find-property (class-name property)
  (%find-place class-name property #'obscure-properties))

(defclass obscure-entity ()
  ((obscure-name :initarg :obscure-name
                 :accessor obscure-name
                 :type string
                 :documentation "The name that is obscured e.g. \"cvm\"")

   (clear-name :initarg :clear-name
               :accessor clear-name
               :type string
               :documentation "The clearname equivilent e.g. net.minecraft.bs")))

(defclass obscure-class (obscure-entity)
  ((obscure-properties :initarg :obscure-properties
                       :accessor obscure-properties
                       :initform (list)
                       :type list
                       :documentation "A list of obscure places on the class.")

   (obscure-methods :initarg :obscure-methods
                    :accessor obscure-methods
                    :initform (list)
                    :type list
                    :documentation "A list of obscure methods on the class.")))

(defclass obscure-place (obscure-entity)
  ((java-type :initarg :java-type
              :accessor java-type
              :type string
              :documentation "The java type of the place.")))

(defclass obscure-method (obscure-entity)
  ((java-type :initarg :java-type
              :accessor java-type
              :type string
              :documentation "The return type of the mehods.")

   (parameters :initarg :parameters
               :accessor parameters
               :initform (list)
               :type list
               :documentation "A list of strings with the parmeter types for the method.")))


(defgeneric add-obscure-entity (entity)
  (:method ((entity obscure-class))
    (setf (gethash (clear-name entity) *class-table*)
          entity)))

(defgeneric add-to-class (class entity)
  (:method ((class obscure-class) (entity obscure-method))
    (push entity (obscure-methods class)))
  (:method ((class obscure-class) (entity obscure-place))
    (push entity (obscure-properties class))))
