#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:unobscure-minecraft)

;;; not sure actually what this should do
;;; should we encapsluate all of the java package on obscure-*
;;; (and then put the java equivelents in the lots of obscure-*)
;;; or do something else?
;;; we also are going to have to think about unecessary hash-table
;;; dispatch that could actually be done at 'compile time'
;;; and then not fuck things up if someone uses load-file

#+ abcl
(defun oclass (class-name)
  (java:jclass (gethash class-name *class-table*)))

#+ abcl
(defun omethod (class-name method-ref &rest parameter-class-refs)
  (let ((class (gethash class-name *class-table*)))
    (apply #'java:jmethod (obscure-name class)
           (obscure-name
            (find method-ref (obscure-methods class) :test #'string= :key #'clear-name))
           parameter-class-refs)))
