#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:unobscure-minecraft.test
  (:use #:cl #:parachute)
  (:export
   #:unobscure-minecraft.test
   #:run
   #:ci-run))

(in-package #:unobscure-minecraft.test)

(defun run (&key (report 'parachute:plain))
  (parachute:test 'unobscure-minecraft.test :report report))

(defun ci-run ()
  (let ((test-result (run)))
    (when (not (null (parachute:results-with-status :failed test-result)))
      (uiop:quit -1))))
