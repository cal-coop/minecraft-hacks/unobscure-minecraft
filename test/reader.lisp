#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:unobscure-minecraft.test)

(define-test unobscure-minecraft.test)

(define-test read-class
  :parent unobscure-minecraft.test

  (let* ((stream (make-string-input-stream "com.mojang.blaze3d.audio.Channel -> cws:"))
         (class (unobscure-minecraft:read-class stream)))
    (is string= "com.mojang.blaze3d.audio.Channel"
        (unobscure-minecraft:clear-name class))

    (is string= "cws"
        (unobscure-minecraft:obscure-name class))))

(defun check-java-place (place clear-name obscure-name java-type)
  (is string= java-type
      (unobscure-minecraft:java-type place))
  (is string= clear-name
      (unobscure-minecraft:clear-name place))
  (is string= obscure-name
      (unobscure-minecraft:obscure-name place)))

(defun check-params (method &rest args)
  (loop :for arg :in args
     :for param :in (unobscure-minecraft:parameters method)
     :do (is string= arg param)))

(define-test read-method
  :parent unobscure-minecraft.test

  ;; method with no args.
  (let* ((stream (make-string-input-stream
                  "    224:224:com.mojang.blaze3d.audio.Listener getListener() -> c"))
         (method (unobscure-minecraft:read-method-or-property stream)))
    (true (typep method 'unobscure-minecraft:obscure-method))
    (check-java-place method "getListener" "c" "com.mojang.blaze3d.audio.Listener")
    (true (null (unobscure-minecraft:parameters method))))

  ;; method with args
  (let* ((stream (make-string-input-stream
                  "    201:205:void convertStereo(java.nio.FloatBuffer,java.nio.FloatBuffer,com.mojang.blaze3d.audio.OggAudioStream$OutputConcat) -> a"))
         (method (unobscure-minecraft:read-method-or-property stream)))
    (true (typep method 'unobscure-minecraft:obscure-method))
    (check-java-place method "convertStereo" "a" "void")
    (check-params method "java.nio.FloatBuffer" "java.nio.FloatBuffer"
                  "com.mojang.blaze3d.audio.OggAudioStream$OutputConcat")))

(define-test read-property
  :parent unobscure-minecraft.test
  
  (let* ((stream (make-string-input-stream
                  "    float gain -> a"))
         (property (unobscure-minecraft:read-method-or-property stream)))
    (true (not (typep property 'unobscure-minecraft:obscure-method)))
    (check-java-place property "gain" "a" "float")))

(define-test read-all
  :parent unobscure-minecraft.test

  (unobscure-minecraft:read-table
   (asdf:system-relative-pathname :unobscure-minecraft "test/table.txt"))

  (true (typep (unobscure-minecraft:find-jmethod "com.mojang.blaze3d.audio.Channel"
                                                "pumpBuffers")
               'unobscure-minecraft:obscure-method))

  (true (typep (unobscure-minecraft:find-property "com.mojang.blaze3d.audio.Channel"
                                                  "source")
               'unobscure-minecraft:obscure-place)))

