#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

;;; read an obscureification table like https://launcher.mojang.com/v1/objects/bd9efb5f556f0e44f04adde7aeeba219421585c2/client.txt

(in-package #:unobscure-minecraft)

(defun read-class (stream)
  (declare (type stream stream))
  (let ((line (read-line stream)))
    (cl-ppcre:register-groups-bind (clear-name obscure-name)
        ("^([\\S]+)\\s+\\-\\>\\s+([^\\s:]+):" line)
      (make-instance 'obscure-class
                     :obscure-name obscure-name
                     :clear-name clear-name))))

(defun read-paremeter-list (string)
  "string should be like: \"foo, bar\""
  (declare (type string string))
  (unless (string= "" string)
      (str:split "," (str:replace-all " " "" string))))

(defun read-method-or-property (stream)
  (let* ((line (read-line stream))
         (place
          (cl-ppcre:register-groups-bind (java-type clear-name obscure-name)
              ("^\\s+([^0-9:\\s]+[^\\s]+)\\s+([\\S]+)\\s+->\\s+([\\S]+)" line)
            (make-instance 'obscure-place
                           :clear-name clear-name
                           :obscure-name obscure-name
                           :java-type java-type)))

         (method
          (cl-ppcre:register-groups-bind (java-type clear-name parameters obscure-name)
              ("^\\s+[0-9:]+(\\S+)\\s+([^(\\s]+)\\(([^\\)]*)\\)\\s+->\\s+([\\S]+)" line)
            ;; split parameters by comma.
            (make-instance 'obscure-method
                           :clear-name clear-name
                           :obscure-name obscure-name
                           :java-type java-type
                           :parameters (read-paremeter-list parameters)))))
    (or place method)))

(defun read-table (pathname)
  (declare (type pathname pathname))
  (with-open-file (s pathname :direction :input)
    (flet ((peek-next-char () (peek-char nil s nil nil t))
           (add-class (class) (add-obscure-entity class) class))
      (let ((previous-class nil))
        (loop :while (peek-next-char) :do
             (alexandria:switch ((peek-next-char) :test #'char=)
               (#\# (read-line s t nil t)) ; comment
               (#\Space (add-to-class previous-class (read-method-or-property s))) ; property/method
               (t (setf previous-class (add-class (read-class s))))
               ))))))
