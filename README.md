# unobscure-minecraft

## getting started

To load the unobscure-table you can use `(unobscure-minecraft:read-table #P"wherever-your-table-is")`

Then you can find references to the method using `omethod` like this

```
CL-USER> (unobscure-minecraft:omethod "com.mojang.blaze3d.audio.Library" "getDebugString")
#<java.lang.reflect.Method public java.lang.String cwt.d() {36140ADD}>
```

you can also use `find-jmethod`, `find-property`.

The library is in very early stages so just do whatever you want with `*class-table*` and the stuff in `unobscure-minecraft.lisp`

if you're wondering what this is, it compliments [abcl in minecraft](https://gitlab.com/cal-coop/abcl-minecraft-again).
